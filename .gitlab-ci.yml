image: docker:latest

workflow:
  rules:
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'
      when: never
    - when: always

services:
  - docker:dind

variables:
  DOCKER_DRIVER: overlay2

stages:
  - build
  - test
  - deploy
  - dast
  - apifuzz
  - cleanup

include:
  # Static
  - template: Jobs/Container-Scanning.gitlab-ci.yml
  - template: Jobs/Dependency-Scanning.gitlab-ci.yml
  - template: Jobs/SAST.gitlab-ci.yml
  - template: Jobs/Secret-Detection.gitlab-ci.yml
  - template: Jobs/SAST-IaC.gitlab-ci.yml
  - template: Jobs/Code-Quality.gitlab-ci.yml
  - template: Security/Coverage-Fuzzing.gitlab-ci.yml
  # Dynamic
  - template: Security/DAST.latest.gitlab-ci.yml
  - template: Security/BAS.latest.gitlab-ci.yml
  - template: Security/DAST-API.latest.gitlab-ci.yml
  - template: API-Fuzzing.latest.gitlab-ci.yml

# Start Application Build and Test
build-simple-notes:
  stage: build
  variables:
    IMAGE: $CI_REGISTRY_IMAGE/$CI_COMMIT_REF_SLUG:$CI_COMMIT_SHA
  before_script:
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
  script:
    - docker build -t $IMAGE .
    - docker push $IMAGE

unit:
  image: python:3.10-bullseye
  stage: test
  variables:
    NOTES_DB_BACKEND: "local"
    RUNNER_GENERATE_ARTIFACTS_METADATA: "true"
    NOTES_DB_DATABASE: "unit-tests"
  before_script:
    - apt update -y; apt upgrade -y
    - apt install gcc curl openssl -y
    - apt-key adv --recv-keys --keyserver keyserver.ubuntu.com 0xcbcb082a1bb943db
    - curl -LsS https://downloads.mariadb.com/MariaDB/mariadb_repo_setup | bash
    - apt install libmariadb3 libmariadb-dev mariadb-client sqlite3 libsqlite3-dev -y
    - pip3 install --upgrade pip; pip3 install -r requirements.txt
  script:
    - python -m unittest tests/test_db.py 2>&1 | tee unit.txt
  artifacts:
    paths:
      - unit.txt

pages:
  image: registry.gitlab.com/pages/hugo/hugo_extended:latest
  stage: build
  before_script:
    - apt update -qq && apt install -y git golang curl bash
  script:
    - hugo --minify -s docs
    - cp -R docs/public .
  artifacts:
    paths:
      - public
  rules:
    - if: $CI_COMMIT_REF_NAME == $CI_DEFAULT_BRANCH
  environment:
    name: documentation
    url: $CI_PAGES_URL
  allow_failure: true
# End Application Build and Test

# Start Deploy and Cleanup
deploy-simple-notes:
  image: registry.gitlab.com/gitlab-org/cluster-integration/helm-install-image:helm-3.10.0-kube-1.24.6-alpine-3.15
  stage: deploy
  variables:
    HELM_HOST: "localhost:44134"
    IMAGE: $CI_REGISTRY_IMAGE/$CI_COMMIT_REF_SLUG:$CI_COMMIT_SHA
    HELM_DEPLOY_NAME: notes-$CI_COMMIT_REF_NAME
    ENV_NAME: $CI_COMMIT_REF_NAME
    ING_PATH: notes-$CI_COMMIT_REF_NAME
  before_script:
    - kubectl config use-context $CI_PROJECT_PATH:simplenotes
    - sh scripts/install_mariadb.sh
    - sh scripts/install_echo.sh
  script:
    - kubectl config use-context $CI_PROJECT_PATH:simplenotes
    - helm upgrade --install $HELM_DEPLOY_NAME chart -f chart/values.yaml
      --namespace notes-app
      --create-namespace
      --set image=$IMAGE 
      --set notes.name=notes-$CI_COMMIT_REF_NAME 
      --set notes.path=$ING_PATH 
      --set notes.dbName=$CI_COMMIT_REF_NAME
      --set notes.env.CI_MERGE_REQUEST_PROJECT_ID=$CI_MERGE_REQUEST_PROJECT_ID
      --set notes.env.CI_MERGE_REQUEST_ID=$CI_MERGE_REQUEST_ID
      --set notes.env.CI_MERGE_REQUEST_SOURCE_BRANCH_SHA=$CI_MERGE_REQUEST_SOURCE_BRANCH_SHA
      --set notes.env.PROJECT_AUTH_TOKEN=$PROJECT_AUTH_TOKEN
      --set restrictednginx.name=restricted-nginx-$CI_COMMIT_REF_NAME
    - kubectl rollout restart deployment notes-$CI_COMMIT_REF_NAME -n notes-app
  after_script:
    - kubectl config use-context $CI_PROJECT_PATH:simplenotes
    - echo "DAST_WEBSITE=http://$(kubectl get svc -n ingress-nginx | grep LoadBalancer | awk '{print $4}')/notes-$CI_COMMIT_REF_NAME" >> deploy.env
    - echo "DAST_API_TARGET_URL=http://$(kubectl get svc -n ingress-nginx | grep LoadBalancer | awk '{print $4}')/notes-$CI_COMMIT_REF_NAME" >> deploy.env
    - export INGRESS_LB_IP=$(kubectl get svc -n ingress-nginx | grep LoadBalancer | awk '{print $4}')
    - echo "INGRESS_LB_IP=$INGRESS_LB_IP" >> deploy.env
    - echo "Access your application at http://$INGRESS_LB_IP/$ING_PATH"
  environment:
    name: $ENV_NAME
    url: http://$INGRESS_LB_IP/$ING_PATH
  artifacts:
    reports:
      dotenv: deploy.env

cleanup-db:
  image: registry.gitlab.com/gitlab-org/cluster-integration/helm-install-image:helm-3.10.0-kube-1.24.6-alpine-3.15
  stage: cleanup
  variables:
    DB_NAME: $CI_COMMIT_REF_NAME
  script:
    - kubectl config use-context $CI_PROJECT_PATH:simplenotes
    - kubectl exec -i deployment.apps/notes-$CI_COMMIT_REF_NAME -n notes-app -- sh scripts/reset_notes_table.sh
  dependencies:
    - dast
    - apifuzzer_fuzz
  rules:
    - if: $CI_COMMIT_REF_NAME != $CI_DEFAULT_BRANCH
# End Deploy and Cleanup

# Start Security Scanner Configurations
semgrep-sast:
  variables:
    SAST_EXPERIMENTAL_FEATURES: "true"
    SCAN_KUBERNETES_MANIFESTS: "false"
    SAST_EXCLUDED_PATHS: ".gitlab, chart, docs, network-policies, scripts, terraform, tests"
  cache: {}

dast:
  stage: dast
  variables:
     DAST_BROWSER_SCAN: "true"
     DAST_BAS_DISABLED: "true"
  rules:
    - if: $CI_COMMIT_REF_NAME == $CI_DEFAULT_BRANCH
      variables:
        DAST_FULL_SCAN_ENABLED: "false"
    - if: $CI_COMMIT_REF_NAME != $CI_DEFAULT_BRANCH
      variables:
        DAST_FULL_SCAN_ENABLED: "true"
  dependencies:
    - deploy-simple-notes

dast_with_bas:
  stage: dast
  variables:
     DAST_BROWSER_SCAN: "true"
  rules:
    - if: $CI_COMMIT_REF_NAME != $CI_DEFAULT_BRANCH
  dependencies:
    - deploy-simple-notes
    - dast
  extends:
    - dast
    - .dast_with_bas_using_services
  services:
    - !reference [.dast_with_bas_using_services, services]
    - name: $CI_REGISTRY_IMAGE/$CI_COMMIT_REF_SLUG:$CI_COMMIT_SHA
      alias: notes-app

dast_api:
  stage: dast
  before_script:
    - sed -i 's@HOST@'"${INGRESS_LB_IP}"'@' test_openapi.v2.0.json
    - sed -i 's@PATH@'"notes-${CI_COMMIT_REF_NAME}"'@' test_openapi.v2.0.json
  variables:
     DAST_API_PROFILE: Quick
     DAST_API_OPENAPI: test_openapi.v2.0.json
  rules:
    - if: $CI_COMMIT_REF_NAME != $CI_DEFAULT_BRANCH

apifuzzer_fuzz:
  stage: apifuzz
  before_script:
    - sed -i 's@HOST@'"${INGRESS_LB_IP}"'@' test_openapi.v2.0.json
    - sed -i 's@PATH@'"notes-${CI_COMMIT_REF_NAME}"'@' test_openapi.v2.0.json
    - export FUZZAPI_TARGET_URL=http://${INGRESS_LB_IP}/
  variables:
    FUZZAPI_PROFILE: Quick-10
    FUZZAPI_OPENAPI: test_openapi.v2.0.json
  rules:
    - if: $CI_COMMIT_REF_NAME != $CI_DEFAULT_BRANCH

gemnasium-python-dependency_scanning:
  image:
    name: $CI_TEMPLATE_REGISTRY_HOST/security-products/gemnasium-python:4-python-3.10
  variables:
    RUNNER_GENERATE_ARTIFACTS_METADATA: "true"
    DS_EXCLUDED_PATHS: "docs"
    GIT_STRATEGY: fetch
  before_script:
    - apt update -y
    - apt install curl -y
    - apt-key adv --recv-keys --keyserver keyserver.ubuntu.com 0xcbcb082a1bb943db
    - curl -LsS https://downloads.mariadb.com/MariaDB/mariadb_repo_setup | bash
    - apt install libmariadb3 libmariadb-dev mariadb-client sqlite3 libsqlite3-dev -y

coverage-guided-fuzzing:
  image: python:latest
  stage: test
  extends: .fuzz_base
  script:
    - pip install --extra-index-url https://gitlab.com/api/v4/projects/19904939/packages/pypi/simple pythonfuzz
    - ./gitlab-cov-fuzz run --engine pythonfuzz -- fuzz.py

kics-iac-sast:
  variables:
    SAST_EXCLUDED_PATHS: ".gitlab, chart/templates/restricted-nginx.yaml, docs, network-policies, notes, scripts, terraform, tests, Dockerfile"

secret_detection:
  variables:
    SECRET_DETECTION_EXCLUDED_PATHS: "docs, tests, scripts, network-policies, terraform"
    GIT_DEPTH: 100

code_quality:
  rules:
    - if: $CODE_QUALITY_DISABLED
      when: never
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
      when: never
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH

code_quality_html:
  extends: code_quality
  variables:
    REPORT_FORMAT: html
  artifacts:
    paths: [gl-code-quality-report.html]
# End Security Scanner Configurations